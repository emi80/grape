.. Grape documentation master file, created by
   sphinx-quickstart on Fri Jan 16 15:06:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========
Grape 2.0
=========

Grape 2.0 provides an extensive pipeline for RNA-Seq analyses. It allows the creation of an automated and integrated workflow to manage, analyse and visualize RNA-Seq data.

Contents:
---------

.. toctree::
   :maxdepth: 2

   quickstart

.. Indices and tables
   ==================
   
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

